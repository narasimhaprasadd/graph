package com.path;

import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class NodeTest {
    @Test
    public void canAddAnAdjacentNodeToNode1() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        node1.addEdge(node2, 1);

        assertTrue(node1.hasPathTo(node2));
    }

    @Test
    public void node1CanHaveMultipleAdjacentPaths() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        Node node4 = new Node("D");
        node1.addEdge(node2, 1);
        node1.addEdge(node3, 1);
        node1.addEdge(node4, 1);

        assertTrue(node1.hasPathTo(node2));
        assertTrue(node1.hasPathTo(node3));
        assertTrue(node1.hasPathTo(node4));
    }

    @Test
    public void AIsConnectedToBAndBIsConnectedToC() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        node1.addEdge(node2, 1);
        node2.addEdge(node3, 1);
        assertTrue(node1.hasPathTo(node3));

    }

    @Test
    public void AIsConnectedToA() {
        Node node1 = new Node("A");
        node1.addEdge(node1, 1);

        assertTrue(node1.hasPathTo(node1));

    }

    @Test
    public void AIsNotConnectedToC() {
        Node node1 = new Node("A");
        Node node3 = new Node("C");


        assertFalse(node1.hasPathTo(node3));

    }

    @Test
    public void shouldGetThePathFromAToC() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        node1.addEdge(node2, 1);
        node2.addEdge(node3, 1);

        Route expectedPath=new Route();
        expectedPath.add(node3,1);
        expectedPath.add(node2,1);
        expectedPath.add(node1,0);
        Route actual=node1.getPathTo(node3);
        actual.display();
        assertEquals(expectedPath,node1.getPathTo(node3));

    }

    @Test
    public void shortestPathFrom_A_to_G_is_A_B_C_F_G() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        Node node4 = new Node("D");
        Node node5 = new Node("E");
        Node node6 = new Node("F");
        Node node7 = new Node("G");
        Node node8 = new Node("H");
        node1.addEdge(node2, 1);
        node2.addEdge(node3, 1);
        node3.addEdge(node4, 1);
        node4.addEdge(node5, 1);
        node3.addEdge(node6, 1);
        node6.addEdge(node7, 1);
        node6.addEdge(node8, 1);

        Route expectedPath=new Route();

        Route actualPath=node1.getPathTo(node7);
        expectedPath.add(node7);
        expectedPath.add(node6,1);
        expectedPath.add(node3,1);
        expectedPath.add(node2,1);
        expectedPath.add(node1,1);
        actualPath.display();
        assertEquals(expectedPath,actualPath);

    }

    @Test
    public void shortestPathFrom_A_to_E_is_A_B_D_E() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        Node node4 = new Node("D");
        Node node5 = new Node("E");
        node1.addEdge(node2, 1);
        node2.addEdge(node3, 1);
        node3.addEdge(node4, 1);
        node4.addEdge(node5, 1);
        node2.addEdge(node4, 1);

        Route expectedPath=new Route();

        expectedPath.add(node1);
        expectedPath.add(node2,1);
        expectedPath.add(node4,1);
        expectedPath.add(node5,1);

        Route route=node1.getPathTo(node5);

       route.display();
        assertEquals(expectedPath, route);

    }

    @Test
    public void shortestPathFrom_A_to_E_is_A_C_E() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        Node node4 = new Node("D");
        Node node5 = new Node("E");
        Node node6 = new Node("F");
        Node node7 = new Node("G");
        node1.addEdge(node2, 1);
        node2.addEdge(node4, 1);
        node4.addEdge(node6, 1);
        node6.addEdge(node7, 1);
        node1.addEdge(node3, 1);
        node3.addEdge(node5, 1);

        Route expectedPath=new Route();

        expectedPath.add(node1);
        expectedPath.add(node3,1);
        expectedPath.add(node5,1);

        Route route=node1.getPathTo(node5);

        route.display();
        assertEquals(expectedPath, route);

    }

    @Test
    public void shortestPathFrom_A_to_E_is_A_F_E() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        Node node4 = new Node("D");
        Node node5 = new Node("E");
        Node node6 = new Node("F");
        node1.addEdge(node2, 1);
        node2.addEdge(node3, 1);
        node3.addEdge(node4, 1);
        node4.addEdge(node5, 1);
        node1.addEdge(node6, 1);
        node6.addEdge(node5, 1);

        Route expectedPath=new Route();

        expectedPath.add(node1,1);
        expectedPath.add(node3,1);
        expectedPath.add(node5);

        Route route=node1.getPathTo(node5);

        route.display();
        assertEquals(expectedPath, route);

    }

    @Test
    public void shortestPathFrom_A_to_E_is_A_F_G_E() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        Node node4 = new Node("D");
        Node node5 = new Node("E");
        Node node6 = new Node("F");
        Node node7 = new Node("G");
        node1.addEdge(node2, 1);
        node2.addEdge(node3, 1);
        node3.addEdge(node4, 1);
        node4.addEdge(node5, 1);
        node1.addEdge(node6, 1);
        node6.addEdge(node7, 1);
        node7.addEdge(node5, 1);

        Route expectedPath=new Route();

        expectedPath.add(node5);
        expectedPath.add(node7,1);
        expectedPath.add(node6,1);
        expectedPath.add(node1,1);

        Route route=node1.getPathTo(node5);

        route.display();
        assertEquals(expectedPath, route);

    }


    @Test
    public void shortestPathFrom_A_to_F_IsA_B_E_F() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        Node node4 = new Node("D");
        Node node5 = new Node("E");
        Node node6 = new Node("F");
        node1.addEdge(node2,2);
        node2.addEdge(node3,2);
        node3.addEdge(node4,2);
        node4.addEdge(node5,2);
        node5.addEdge(node6,2);
        node2.addEdge(node5,2);

        Route expectedPath=new Route();

        expectedPath.add(node6);
        expectedPath.add(node5,2);
        expectedPath.add(node2,2);
        expectedPath.add(node1,2);

        Route route=node1.getPathTo(node6);

        route.display();
        assertEquals(expectedPath,route);

    }

    @Test
    public void shortestPathFrom_A_to_F_IsA_B_C_D_E_F() {
        Node node1 = new Node("A");
        Node node2 = new Node("B");
        Node node3 = new Node("C");
        Node node4 = new Node("D");
        Node node5 = new Node("E");
        Node node6 = new Node("F");
        node1.addEdge(node2,2);
        node2.addEdge(node3,2);
        node3.addEdge(node4,2);
        node4.addEdge(node5,2);
        node5.addEdge(node6,2);
        node2.addEdge(node5,10);

        Route expectedPath=new Route();

        expectedPath.add(node6);
        expectedPath.add(node5,2);
        expectedPath.add(node4,2);
        expectedPath.add(node3,2);
        expectedPath.add(node2,2);
        expectedPath.add(node1,2);

        Route route=node1.getPathTo(node6);

        route.display();
        assertEquals(expectedPath,route);

    }
}
