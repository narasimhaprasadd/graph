package com.path;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class PathFinderTest {


    @Test
    public void shouldInitializeNodes() {
        PathFinder pathFinder=new PathFinder();
        pathFinder.initialize();

    }


    @Test
    public void shouldBeAbleToFindPathFromEToA() {
        PathFinder pathFinder=new PathFinder();
        assertTrue(pathFinder.pathExist("E","A"));

    }

    @Test
    public void shouldNotBeAbleToFindPathFromAToE() {
        PathFinder pathFinder=new PathFinder();
        assertFalse(pathFinder.pathExist("A","E"));

    }


    @Test
    public void shouldBeAbleToFindPathFromAToA() {
        PathFinder pathFinder=new PathFinder();
        assertTrue(pathFinder.pathExist("A","A"));
    }


    @Test
    public void shouldNotBeAbleToFindPathFromAToD() {
        PathFinder pathFinder=new PathFinder();
        assertTrue(pathFinder.pathExist("A","A"));
    }


}
