package com.path;

import java.util.*;

public class Node {

    private List<Node> adjacentNodes;
    private List<Integer> adjacentNodesWeight;
    private String name;
    private boolean visited = false;

    public Node(String name) {
        adjacentNodes = new ArrayList<>();
        adjacentNodesWeight = new ArrayList<>();
        this.name = name;
    }

    public void addEdge(Node node, int weight) {
        adjacentNodes.add(node);
        adjacentNodesWeight.add(weight);
    }


    public boolean hasPathTo(Node destination) {
        List<Node> visitedNode = new ArrayList<>();
        return this.hasPath(destination, visitedNode);

    }

    public boolean hasPath(Node destination, List<Node> visitedNode) {
        visitedNode.add(this);

        if (adjacentNodes.contains(destination)) {
            return true;
        }
        for (Node node : adjacentNodes) {
            if (visitedNode.contains(node)) {
                continue;
            }
            if (node.hasPath(destination, visitedNode)) {
                return true;
            }
        }

        return false;
    }


    public Route getPath(Node destination, Route path, List<Node> visitedNode) {
        visitedNode.add(this);

        if (adjacentNodes.contains(destination)) {
            path.add(destination,getDestinationWeight(destination));
            return path;
        }
        for (Node node : adjacentNodes) {
            if (visitedNode.contains(node)) {
                continue;
            }
            Route newPath = node.getPath(destination, path, visitedNode);
            if (newPath.contains(destination)) {
                newPath.add(node,getDestinationWeight(node));
                return newPath;
            }
        }

        return path;
    }

    private int getDestinationWeight(Node destination) {
        int indexOfWeight = adjacentNodes.indexOf(destination);
        return adjacentNodesWeight.get(indexOfWeight);
    }


    public Route getPathTo(Node destination) {
        List<Node> visitedNode = new ArrayList<>();
        Route path = new Route();
        List<Route> allPaths = new ArrayList<>();
        path = getPath(destination, path, visitedNode);
        path.add(this);
        Route shortestroute = new Route(path);
        allPaths.add(shortestroute);
        //path.deQue();
        do {
            visitedNode.clear();
            Node lastNode = path.deQue();
            visitedNode.add(lastNode);
            Route newPath = new Route();
            getAlternatePath(destination, visitedNode, allPaths, lastNode, newPath);
        } while (path.nodeSize() > 0);

        return getLowestWeight(allPaths, shortestroute);
    }


    private void getAlternatePath(Node destination, List<Node> visitedNode, List<Route> allPaths, Node lastNode, Route newPath) {
        if (hasPath(destination, visitedNode)) {
            visitedNode.clear();
            visitedNode.add(lastNode);
            Route anotherPath = getPath(destination, newPath, visitedNode);
            anotherPath.add(this);
            allPaths.add(anotherPath);
        }
    }

    private Route getShortestHops(List<Route> allPaths, Route shortestroute) {
        int shortestDistance = shortestroute.nodeSize();
        for (Route route : allPaths) {
            if (route.nodeSize() < shortestDistance) {
                shortestDistance = route.nodeSize();
                shortestroute = route;
            }
        }
        return shortestroute;
    }

    private Route getLowestWeight(List<Route> allPaths, Route shortestroute) {
        int shortestDistance = shortestroute.totalWeight;
        for (Route route : allPaths) {
            if (route.totalWeight < shortestDistance) {
                shortestDistance = route.totalWeight;
                shortestroute = route;
            }
        }
        return shortestroute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        if (visited != node.visited) return false;
        if (adjacentNodes != null ? !adjacentNodes.equals(node.adjacentNodes) : node.adjacentNodes != null)
            return false;
        if (adjacentNodesWeight != null ? !adjacentNodesWeight.equals(node.adjacentNodesWeight) : node.adjacentNodesWeight != null)
            return false;
        return name != null ? name.equals(node.name) : node.name == null;

    }

    @Override
    public int hashCode() {
        int result = adjacentNodes != null ? adjacentNodes.hashCode() : 0;
        result = 31 * result + (adjacentNodesWeight != null ? adjacentNodesWeight.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (visited ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return name;
    }
}
