package com.path;

import java.util.ArrayDeque;
import java.util.Deque;

public class Route {
    Deque<Node> nodes;

    int totalWeight;
    public Route(Route path) {
        nodes=new ArrayDeque<>(path.nodes);
        totalWeight=path.totalWeight;
    }

    public Route() {
        nodes = new ArrayDeque<>();
    }

    public void add(Node node,int weight) {
        nodes.push(node);
        totalWeight+=weight;
    }
    public boolean contains(Node node) {
        return nodes.contains(node);
    }
    public Node pop() {
        return nodes.pop();
    }
    public Node deQue(){
        return nodes.removeLast();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Route route = (Route) o;

        if (totalWeight != route.totalWeight) return false;
        return nodes.containsAll(((Route) o).nodes);

    }

    @Override
    public int hashCode() {
        int result = nodes != null ? nodes.hashCode() : 0;
        result = 31 * result + totalWeight;
        return result;
    }

    public int nodeSize() {
        return nodes.size();
    }

    public void display() {
        while(!nodes.isEmpty()) {
            System.out.println((nodes.pop()).toString());
        }
        System.out.println("Total weightage:" +totalWeight);
    }

    public void add(Node node) {
        nodes.push(node);

    }
}
