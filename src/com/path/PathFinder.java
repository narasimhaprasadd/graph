package com.path;

import java.util.*;

public class PathFinder {
    List adjacentNodes;
    HashMap<String, List<String>> edges;
    List<String> nodesVisited;

    public PathFinder() {
        initialize();
    }

    public boolean pathExist(String source, String destination) {
        Queue<String> queue = new LinkedList();
        queue.add(source);
        markVisited(source);
        while (!queue.isEmpty()) {
            String currentNode = queue.remove();
            List<String> adjacentNodes = edges.remove(currentNode);
            for (String node : adjacentNodes) {
                if (node == destination) {
                    return true;
                }
                if (!nodesVisited.contains(node)) {
                    markVisited(node);
                    queue.add(node);
                }
            }
        }
        return false;
    }

    private List getNewNode(String currentNode) {
       return edges.get(currentNode);
    }

    private void markVisited(String node) {
        nodesVisited.add(node);
    }

    public void initialize() {
        edges = new HashMap<>();
        adjacentNodes = new LinkedList<String>();
        nodesVisited = new ArrayList();
        edges.put("A", Arrays.asList("F"));
        edges.put("B", Arrays.asList("A"));
        edges.put("H", Arrays.asList("B"));
        edges.put("B", Arrays.asList("C"));
        edges.put("C", Arrays.asList("D"));
        edges.put("D", Arrays.asList("E"));
        edges.put("C", Arrays.asList("E"));
        edges.put("E", Arrays.asList("B"));
    }
}
